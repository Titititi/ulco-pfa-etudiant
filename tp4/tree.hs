
data Tree a = Leaf | Node a (Tree a) (Tree a) -- deriving Show

instance Show a => Show (Tree a) where
    show Leaf = " "
    show (Node r g d) = "Node" ++ show (r) ++ show (g) ++ show (d)

instance Foldable Tree where
    foldMap f Leaf = mempty
    foldMap f (Node x l r) = f x `mappend` foldMap f l
                                 `mappend` foldMap f r

                                 
mytree1 :: Tree Int
mytree1 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

main = do
    print mytree1
    -- print mytree2
    -- print $ sum mytree1
    -- print $ maximum mytree1

